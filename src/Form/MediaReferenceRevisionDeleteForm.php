<?php

namespace Drupal\media_reference_revisions\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Media reference revision entities.
 *
 * @ingroup media_reference_revisions
 */
class MediaReferenceRevisionDeleteForm extends ContentEntityDeleteForm {
}
